# Reunión de Trabajo BachacoVE
---
**Lugar:** Sala de reuniones Dirección de Tecnología Información Y Comunicación UCV.

**Fecha y hora:** Viernes 15 de Abril del 2016 9:00 hrs.

**Asistentes:** Victor Inojosa, Jorge Escalona, Francisco Palm, Samuel Pavón, Rafael Torrealba, Juan Pablo Rodríguez.

## Puntos de agenda

* Comunicar la Minuta de la reunión previa en el CFG.
* Debate de Impresión sobre la Reunión Previa.
* Acuerdos comerciales, estrategia y plan de acción.
* Censo de integrantes de BachacoVE.



### Debate de impresión

La reunión comenzó con la lectura de la minuta de la reunión anterior y aclaratoria por parte de los asistentes a esa reunión, de diversas interrogantes y detalles que pudieron quedar en el aire o fuera de la minuta, se recaló el hecho de que la única forma de interactuar con el CFG es mediante la figura jurídica de cooperativa, por consiguiente, se acordó asimilar la lógica detrás de las cooperativas mediante la lectura de la ley y el reglamento de sociedades cooperativas y el asesoramiento continuo de los compañeros que ya se encuentren haciendo vida dentro de una cooperativa. Se propuso incluir fuertemente en el debate de la estructura formativa planteada en nuestro proyecto el tema de la comuna "Mano Vuelta TIL" y también se recalco que el tema formativo debe ser integral y autosustentable y que las estrategias en cuanto este tema no deben estar ligadas a la línea de una institución.

### Acuerdos Comerciales, Estrategia y plan de Acción.

En este apartado se discutió como debíamos abordar el tema de la negociación con el CFG para la obtención de los recursos necesarios para el apalancamiento del proyecto, Juan Pablo expuso las experiencias que ha tenido trabajando en proyectos para en el CFG y su paso por el órgano integrador de cooperativas impulsado directamente por Guy Vernaez (Secretario ejecutivo del Consejo Federal de Gobierno), aquí el compañero Juan Pablo expuso sus buenas relaciones pasadas y actuales con Guy, sin embargo sus relaciones con el Órgano Integrador (OI) no han gozado de la misma condición, manifestando que el OI ha mantenido prácticas poco éticas, maliciosas y deshonestas en sus relaciones con otras cooperativas y actores; pero,  a pesar que este órgano goza del apoyo irrestricto de Guy, la realidad es que las cooperativas que conforman ese órgano no están dando la respuesta técnica suficiente para suplir las necesidades del CFG y por lo tanto se ven obligados a buscar apoyo en otras estructuras como por ejemplo la nuestra. Ante esto se plantean dos escenarios: 

1. que Guy insista en que nos integremos a este OI y quedaría de nosotros infiltrarnos en la estructura de toma de decisiones de ese órgano para lograr unas condiciones de trabajo equitativas y justas.
2. que no sea necesario integrarnos al órgano y que mediante buenas practicas de trabajo, productividad y calidad de nuestros proyectos podamos conformar una estructura paralela robusta que sume mas voluntades.

Indistintamente de los escenarios se acordó que en la mesa de negociación con Guy se acepten las siguientes condiciones que se plantearon en primer momento:

* Conformarnos en cooperativas ya sean de primer o segundo nivel (de segundo nivel en caso de armar la estructura que compita con el OI).

* Llevar contabilidad abierta en nuestro ejercicio económico.

* Desarrollar los módulos particulares que necesiten en el CFG.


Por contraparte en la relación que acordemos, el CFG deberá respetar los siguientes principios:

* Financiar la plataforma de formación independientemente de los otros financiamientos que se consigan.

* Que al menos una Unidad Productiva se dedique exclusivamente al desarrollo del núcleo del PRO.

* Garantizar la liberación de código no sensible.

Queda por negociar:

* La cantidad de unidades productivas que recibirán en una primera fase el financiamiento (en un cálculo realista la propuesta que llevaremos sera de 5 UP).

* Que todo el apalancamiento inicial se gestione mediante la cooperativa Juventud Productiva y la cooperativa Saní (representadas por los compañeros Juan pablo y Francisco respectivamente) ya que se encuentran legalmente constituidas y que luego los recursos sean distribuidos para sostener la formalización  de las 3 UP restantes en tres nuevas cooperativas y a su vez en paralelo formalizar una cooperativa de segundo nivel que agrupe en una estructura integradora a todas las Unidades Productivas de BachacoVE.

* Hacer ver a Guy en las próximas reuniones que el proyecto PRO y el la parte formativa son valores agregados de los proyectos particulares que tiene que asumir el CFG.

### Censo de integrantes de BachacoVE.

Se acordó definir los mecanismos para censar a las personas que están dispuestos a integrarse productivamente al proyecto para integrarlas en las Unidades productivas y a su vez si estas unidades son sensibles a conformar el bloque de las primeras 5 cooperativas a apalancar en la primera fase. En paralelo con el censo de las personas también hacer una revisión de los desarrollos o proyectos que se encuentren andando dentro de BachacoVE de manera que puedan ser aprovechados para nutrir el mega-proyecto.

## Acuerdos y Estrategias a lo interno.


### A corto plazo.

* Fijar una reunión con Guy para la próxima semana para negociar todo los puntos antes expuestos (tentativamente el Miércoles en la tarde).

* Definir los puntos concretos y las estrategias de cara a esa reunión.

* Activar pro-activamente todos los canales de comunicación de BachacoVE.

* Realizar el Censo de personas que expresen su compromiso productivo con el proyecto.

* Organizar los grupos que conformaran las UP's.

* Depurar la el grupo de telegram y la lista de correo con tareas concretas para los integrantes.

### A mediano plazo

* Socializar la ley, reglamento y documento constitutivo de las cooperativas, realizar mini talleres con el compañero Juan Pablo que ya ha superado la curva de aprendizaje en cuando al funcionamiento de esta estructura.

* Creación de cooperativas de primer y segundo nivel.

* Debatir sobre la creación de las estructuras internas de bachacoVE: de toma de decisiones, organizacional y técnica.

* Definir el plan de Acción para BachacoVE para los próximos años.





