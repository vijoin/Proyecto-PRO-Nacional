Reducción del costo de la hora de asesoría
Reducción a cero los vehículos
Reducción a cero los seguros de vehículos
Reducción a cero la inversión inmobiliaria
Reducción de viáticos
Reducción de pasajes
Reducción a la mitad de la inversión de valores
Reducción de la cantidad de las computadoras de escritorio
Reducción del costo de cada computadora de escritorio
Reducción de la cantidad de laptops
Reducción del costo de las laptops
Reducción del costo de los servidores
Reducción a la mitad del costo de la plataforma formativa
Reducción del costo de cada contenido formativo
Reducción del costo de cada foro
Reducción del costo de la campaña de divulgación
Reducción de la cantidad de las cayapas
reducción del costo de cada cayapa

Se eliminó el censo nacional
se eliminaron 6 contenidos formativos
Se quitaron dos módulos transversales
Se eliminó la malla curricular
Se eliminió la norma técnica

Se plantea el proyecto en 2 etapas
	una etapa de arranque, con requerimientos mínimos
	una etapa de apalancamiento a las unidades productivas conformadas para que formen otras unidades productivas, tanto organizativa como productivamente

---

#Cambios el 04/10/2016

##Identificación
+ Cambiado nombre de razón social a fundación ATTA y borrado el rif anterior, esperando número de rif nuevo
+ Actualizada la Descripción del proyecto a desarrollar consona con las nuevas reducciones
+ Colocado nombre del proyecto.
+ Modificada la descripcion de la potencialidad social de proyecto

##producto Desarrollo de Software
+ Suprimido gastos de uniforme
+ Reducido de 30 a 20 numero de personal profesional y HCM
+ Reducido a de 35 a 10 cantidad de equipos de escritorio (1 por cada 2 personas)
+ Reducido de 35 a 20 cantidad de laptops (uno por persona)
+ Actualizado el costo de las laptops, desktop y servidores a la realidad actual
+ Reducida las cantidades de los items de 3 up a 2 up.
+ Reducido el monto de materiales y suministros.
+ Reducida la cantidad de viáticos de 90 a 60.
+ Reducido los demas gastos admistrativos acorde a la reduccion de up.
+ Reducido a 0 la cantida de vehiculos.
##Implementación de Sistemas
+ Suprimido gastos de uniforme.
+ Reducido el numeto de equipos de desktop a 5 (1 por cada 2 personas)
+ Actualizados los precios de los desktops laptop y servidores a la realidad actual
+ Reducido el monto de materiales y suministros.
+ Removido el vehiculo de la estructura de costo
##Mantenimiento y soporte de sistemas
+ Actualizado precios de equipos desktop, laptop y servidores a la realidad actual
+ Reduccion de numero de equipos desktop a 5 (1 por cada 2 personas)
+ Suprimida costo de oficina
+ Suprimida costo de vehiculo
##Formación y capacitación
+ Actualizado precios de equipos desktop, laptop y servidores a la realidad actual
+ Reduccion de numero de equipos desktop a 5 (1 por cada 2 personas)
+ Suprimida la plataforma MOOC

#Cambios al 05/10/2016


##Identificación
+ Actualizado tiempo de ejecución a 24 meses
+ Borrado de los productos "7 contenidos estratégicos para formación masiva a distancia"
+ Borrado de los productos "5000 formaciones"
+ Borrado de los productos "1 Censo Nacional de Capacidades para el Desarrollo de programas informáticos"
+ Borrado de los productos "2 Cayapas de organización, codificación y difusión de avances del proyecto"
+ Borrado de los productos "10 Foros Universitarios para la difusión y la captación de talentos"
+ Borrado "1 propuesta de malla curricular adaptada a las necesidades de desarrollo actuales para las Universidades"

##Producto Desarrollo de Software
+ Reducido el numero de personas de 20 a 10
+ Reducidas horas de asesoramiento a 50
+ Suprimidas los equipos de escritorio
+ Actualizado las cantidades de gastos y costos a 1 por cada item excepto servidores que quedan en 2
+ Reducido viáticos a 5 por mes
+ Reducido Pasajes a 1 por mes
+ Reducido monto de inmobiliario

##Producto Implementación de sistemas
+ Reducido horas de asesoria a 20
+ Suprimido los equipos de escritorio
+ Viáticos reducido a 3
+ Pasajes reducido a 3
+ Reducido monto de inmobiliario

##Mantenimiento y Soporte de Sistemas
+ Reducido número de personas a 5
+ Reducido horas de asesoria a 10
+ Suprimido los equipos de escritorio
+ Reducido el numero de laptops a 5
+ Viáticos reducido a 3
+ Pasajes reducido a 3
+ Reducido monto de inmobiliario

#Cambios al 06/10/2016
#Identifcación
+ Agregado número de rif
+ Suprimido "1 o varias propuestas de normas técnicas sobre sistemas del Estado"

#cambios al 09/10/2016

+ Actualizada la ficha resumen con los nuevos montos.
+ Actualizada la celda en la identificación del formato instrumento para que mostrata el monto total del proyecto correctamente

#cambios al 11/10/2016

+ Actualizada la sinopsis del PRO para corresponder con los recortes