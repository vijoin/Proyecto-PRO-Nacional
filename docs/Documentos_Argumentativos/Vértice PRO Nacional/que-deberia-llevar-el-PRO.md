#Propuesta de lo que debe Contener un ERP libre

Levantamiento de información con de los módulos necesarios para los procedimientos administrativos, manuales y automatizados y  para asegurar, un flujo de información adecuado, actualizado y oportuno que   sirva de soporte en la toma de decisiones de cada uno de los niveles de la organización, con el fin de mejorar la gestión administrativa, financiera y no financiera, de gobierno central,  regional, local y consejos comunales.

Estaria compuesto por los Sub-sistemas siguientes:
* Contabilidad Presupuestaria 
* Formulación de Presupuesto 
* Contabilidad de financiera
* Obras y Contratos
* Tesorería
* Bienes nacionales 
* Nómina y Personal
* Compras y Almacén
* Documentos y correspondencias 
* Atención al Ciudadano
* Hacienda Municipal 
* Hacienda pública Estadal 
* Créditos y Cobranzas
* * Pequeña y Mediana Empresa 
* * Vivienda
* * Agrícolas
* * Consejos Comunales
* Control de Viáticos
* Contrataciones publicas

Las características particulares de cada uno de estos sub-sistemas que deberían tener se describen seguidamente:

## CONTABILIDAD PRESUPUESTARIA

Este sub-sistema debe tener una los procesos de la  distribución institucional del presupuesto de gastos, clasificando los créditos presupuestarios, de acuerdo con la responsabilidad de las unidades en el cumplimiento de los objetivos y metas de cada programa o programa o proyecto. Permite mantener un estricto control en la ejecución y ordenación de los compromisos causados, pagos, traslados, adiciones/disminuciones y ajustes de ejecución, que facilita la toma de decisiones durante todas las etapas del proceso administrativo 

Este sub-sistema deberá generar la información siguiente:

* Definiciones Específicas 
* * Configuraciones Generales 
* * Niveles Presupuestarios 
* * Títulos presupuestarios
* * Tipos de Financiamiento 
* * Asignación Inicial
* * Asignación Fuente de Financiamiento
* * Asignar Partidas
* * Artículos de ley
* * Aprobación 
* * Crear Títulos
* * Documentos
* * * Pre compromisos
* * * Compromisos
* * * Causados
* * * Pagos
* * * Ajustes

* Ejecución Presupuestaria
* * Ejecución Global 
* * Ejecución Global por Financiamiento
* * Pre comprometer
* * Comprometer
* * Causar 
* * Pagar
* * Solicitud de traslados
* * Traslados
* * Solicitud de Créditos Adicionales/Disminuciones
* * Créditos Adicionales /Disminuciones
* * Ajustes Ejecución
* * Aprobación del Compromiso 
* * Distribución por Fuentes de Financiamiento de los Movimientos

* Reportes 
* * Definiciones Específicas
* * Catálogo de Partidas
* * Documentos Pre compromisos 
* * Documentos Compromiso
* * Documentos Causados
* * Documentos Pagos 
* * Documentos Ajuste
* * Tipos de Financiamiento
* * Artículos  de Ley

* Movimientos
* * Certificación Presupuestaria
* * Pre compromisos
* * Causados
* * Pagados
* * Traslados
* * Ajustes 
* * Créditos Adicionales/Disminuciones(Por Aprobación)
* * Rectificaciones del Presupuesto 
* * Créditos Adicionales(según oficio)
* * Créditos Adicionales(según decreto)

* Ejecución Presupuestaria
* * Asignación Inicial Consolidado 
* * Disponibilidad presupuestaria
* * Disponibilidad presupuestaria Resumida
* * Ejecución del Presupuesto
* * Gasto Ejecutado por Parte. Y Genéricas (BCV)
* * Resumen General de la Ejec. Presup. Por Partidas
* * Maestro de Presupuesto
* * Ejecución Presupuestaria(Pre Compromisos)
* * Ejecución Presupuestaria por Partidas
* * Detalles de Compromisos
* * Detalles de Causados
* * Registro de la Ejec. Finan. Del Presup. de gastos
* * Relación diaria de compromisos
* * Disponibilidad Financiera
* * Disponibilidad Negativa 
* * Consolidado
* * de Ejecución por Movimientos
* * Ejecución Presupuestaria Coordinado (Dispon.Compr)

* Mantenimiento
* * Cerrar Etapa de Definición 
* * Cierre de Periodo
* * Cambiar Contraseña