Buenas tardes.

La presente es para informarles que la Ruta de Exploración de Odoo Recursos Humanos, se culmino el miércoles pasado.

Es importante indicar que lo que se perseguía en esta fase exploratoria, es que se tuviese un conocimiento básico de los módulos de la aplicación Odoo, que nos pudiese permitir, el tener una idea de que aspectos de nuestra gestión diaria quedaría cubierta con esta aplicación y que aspectos tendrían que ser desarrollados como requerimientos.

Estamos muy claros que no se revisaron una serie de Procesos que  forman parte de nuestras actividades usuales, pero también que a simple vista no se detectaron en Odoo, en estos aspectos hay que profundizar mas en la aplicación a ver como pudiesen ser tratados .

Caso Liquidaciones,Vacaciones,Calculo de Fideicomiso etc.

Por otro lado revisamos que aspectos del Modulo de asistencia e inasistencia, pueden ser de mucha utilidad.

En general estamos consciente de que se requiere una etapa de mayor profundidad para poder tener una conclusión clara en todos estos aspectos.

Algunos productos que se generaron de esta Fase Exploratoria, son los siguientes:

Diagrama de Proceso de Calculo de Nomina.
Diagrama de Proceso de Reclutamiento y Selección.
Listado de Requerimientos Básicos detectados(algunos de ellos posiblemente requieren una mayor investigación en Odoo).
Diseño preliminar de un formato para las convenciones acordadas.
Algunos formatos utilizados en el proceso de Reclutamiento y selección.

Estaremos indicando los próximos pasos.
