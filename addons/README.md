# Análisis de Aplicaciones y/ Módulos

## Base
* Distribución Politico-Territorial (l10n_ve_dpt)
* Herencia a res.partner
* Multi-Sede
* Reglas de Registro Multisede
    
## Contabilidad
* Plan contable para gobierno (l10n_ve_chart_account_gov)
* Retenciones

## Planificación
* Plan Operativo Anual

## Presupuesto
* Formulacion Física
* Formulación Financiera
* Formulación Administrativa
* Modificación Física
* Modificación Financiera
* Modificación Administrativa
* Ejecución Física
* Ejecución Financiera
* Ejecución Administrativa

## Compras
* Workflow de Compras: Precompromiso, compromiso, ejecución
* Retenciones
* Gestión de Proveedores
* Consulta de Status de Proveedores en SNC

## Inventario
* Reglas de Abastecimiento
* Múltiples Almacenes

## Talento Humano
* Reclutamiento
* Selección
* Contratación
* Liquidación
* Nómina
* Constancia de Trabajo
* Control de Asistencia
* Control de Ausencias (Vacaciones, Permisos, Reposos)
* Obligaciones Patronales (FAOV, IVSS, etc)
* Declaración de ISLR del Trabajador
* Fideicomiso
* Seguro Autoadministrado
* Objetivos de Desempeño Individual
* Encuestas

## Atención al Ciudadano
* Gestión de Ayudas
* Seguimiento de Ayudas
* Bus de interoperabilidad para consulta de Ayudas otorgadas por otros entes
* Gestión de Comunicación con el ciudadano